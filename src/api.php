<?php
//----------------------------------------Base----------------------------------------------
function get_dinosaurs()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return array();
}

function get_dinosaur_by_slug($slug)
{
    $url = sprintf("https://allosaurus.delahayeyourself.info/api/dinosaurs/%s", $slug);
    $response = Requests::get($url);
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return null;
}

function get_top_rated_dinosaurs()
{
    $dinosaurs = get_dinosaurs();
    $keys = array_rand($dinosaurs, 3);
    $top = array();
    foreach($keys as $key)
    {
        $top[] = $dinosaurs[$key];
    }
    return $top;
}
//----------------------------------------Terran----------------------------------------------
function get_terran()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/starcraft/terran/units/');
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return array();
}

function get_terran_by_slug($slug)
{
    $url = sprintf("https://allosaurus.delahayeyourself.info/api/starcraft/terran/units/%s", $slug);
    $response = Requests::get($url);
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return null;
}

function get_abilities_terran()
{
    $terrans = get_terran();
    $keys = array($terrans);
    $top = array();
    foreach($keys as $key)
    {
        $top[] = $terrans[$key];
    }
    return $top;
}

function get_top_rated_terran()
{
    $terrans = get_terran();
    $keys = array_rand($terrans, 3);
    $top = array();
    foreach($keys as $key)
    {
        $top[] = $terrans[$key];
    }
    return $top;
}
//----------------------------------------ZERG----------------------------------------------
function get_zerg()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/starcraft/zerg/units/');
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return array();
}

function get_zerg_by_slug($slug)
{
    $url = sprintf("https://allosaurus.delahayeyourself.info/api/starcraft/zerg/units/%s", $slug);
    $response = Requests::get($url);
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return null;
}

function get_abilities_zerg()
{
    $dinosaurs = get_zerg();
    $keys = array_rand($dinosaurs, 3);
    $top = array();
    foreach($keys as $key)
    {
        $top[] = $dinosaurs[$key];
    }
    return $top;
}

function get_top_rated_zerg()
{
    $zergs = get_zerg();
    $keys = array_rand($zergs, 3);
    $top = array();
    foreach($keys as $key)
    {
        $top[] = $zergs[$key];
    }
    return $top;
}