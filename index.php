<?php 
//**************************************************************************************//
//                      Fait par ANTOINE DURAND et THOMAS BOYER
//**************************************************************************************//
//----------------------------------------Params----------------------------------------------
require "vendor/autoload.php";
use Michelf\Markdown;

$loader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/views');

$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', '\Twig\Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new \Twig\Extension\DebugExtension()); // Add the debug extension
    
    $twig->addFilter(new \Twig\TwigFilter('markdown', function($string){
        return Markdown::defaultTransform($string);
    }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

//----------------------------------------Terran----------------------------------------------
Flight::route('/terran', function(){
    $data = array(
        'terrans' => get_terran(),
    );
    
    Flight::render('indexTerran.twig', $data);
});

Flight::route('/terran/@slug', function($slug){
    $data = array(
        'terran' => get_terran_by_slug($slug),
        'top_rated_tarren' => get_top_rated_terran(),
    );

    Flight::render('terran.twig', $data);
});
//----------------------------------------Zerg----------------------------------------------
Flight::route('/zerg', function(){
    $data = array(
        'zergs' => get_zerg(),
    );
    
    Flight::render('indexZerg.twig', $data);
});

Flight::route('/zerg/@slug', function($slug){
    $data = array(
        'zerg' => get_zerg_by_slug($slug),
        'top_rated_zerg' => get_top_rated_zerg(),
    );

    Flight::render('zerg.twig', $data);
});
//----------------------------------------Protoss----------------------------------------------
Flight::route('/protoss', function(){
    $data = array(
        'protosss' => get_terran(),
    );
    
    Flight::render('indexProtoss.twig', $data);
});

Flight::route('/Protoss/@slug', function($slug){
    $data = array(
        'protoss' => get_terran_by_slug($slug),
        'top_rated_protoss' => get_top_rated_protoss(),
    );

    Flight::render('protoss.twig', $data);
});
//----------------------------------------Base----------------------------------------------
Flight::route('/', function(){
    $data = array(
        'dinosaurs' => get_dinosaurs(),
    );
    Flight::render('home.twig', $data);
});
//----------------------------------------Start----------------------------------------------
Flight::start();